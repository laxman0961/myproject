import React, { Component } from 'react';
import Test from './scr/Test.jsx';
import store from './scr/TestStore';
import Header from './scr/Header.jsx';
import Home from './scr/Home.jsx';
import About from './scr/About.jsx';
import Detail from './scr/Detail.jsx';
import StatePage from './scr/StatePage.jsx';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';


class App extends Component {
   render(){
      return(
         //<Test store={store}/>
         <Router>
            <div>
               <Header />
               <switch>
                  <Route path="/" exact component={Home} />
                  <Route path="/about" exact component={About} />
                  <Route path="/State" exact component={StatePage} />
                  <Route path="/about/:id" component={Detail} />
                  <Route path="/mobx" component= {()=> <Test store={store}/>} />
               </switch>
            </div>
         </Router>
      );
   }
}
export default App;
export { Test };