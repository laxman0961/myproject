import React, { Component } from 'react';
import { observer } from "mobx-react"
import store from './TestStore';

class StateDetail extends Component{

   render() {
      const { name, children } = this.props
      return(
         <div>
            <h2>{name}</h2>
            <h2>{children}</h2>
         </div>
      );
   }
}
export default StateDetail;