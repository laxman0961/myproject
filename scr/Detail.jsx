import React, { Component, useEffect } from 'react';
import { observer } from "mobx-react"
import { Link } from 'react-router-dom'
 
@observer
class Detail extends Component{
   // async componentDidMount () {
   //    const url = "https://fortnite-api.theapinetwork.com/upcoming/get"
   //    const response = await fetch(url)
   //    const data = await response.json()
   //    console.log(data)
   // }
   constructor(props) {
      super(props);
  
      this.state = {
        data: [],
        isLoaded: false
      };
    }
  
    componentDidMount() {
      fetch('https://fortnite-public-api.theapinetwork.com/prod09/upcoming/get')
        .then(response => response.json())
        //.then(data => console.log("pml",data.items))
        .then(items => this.setState({
           data: items.items,
            isLoaded: true
        }
        ))
    }
    render(){
       const { data, isLoaded } = this.state
       console.log("data :------------------",data)
       console.log(isLoaded)
       if(!isLoaded){
          return <div>data loading....</div>
       }
       else{
          return(
             <div>
                <h2>data Loaded</h2>
                <ul>
                {data.map(item=>
                  (
                   <li>
                     <Link to={`about/${item.itemid}`} >{item.itemid}</Link>
                   </li>
                   )
                  )}
                </ul>
             </div>
          );
       }
   }
}
export default Detail;