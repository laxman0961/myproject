import React, { Component } from 'react';
import StateDetail from './StateDetail.jsx';
import Home from './Home.jsx';

class StatePage extends Component{
   state = {
         // student: [
         //    {
         //       Name:"pml",
         //       Mark:900,
         //       Grade: "B"
         //    },
         //    {
         //       Name:"lax",
         //       Mark:1100,
         //       Grade: "S"
         //    },
         //    {
         //       Name:"suresh",
         //       Mark:750,
         //       Grade: "D"
         //    }
         // ]

         Name: "pml0961"
      };
      changeMark = () => {
         console.log("i am clicked")
         this.setState({
            student: [
               {
                  Name:"pml",
                  Mark:901,
                  Grade: "B"
               },
               {
                  Name:"lax",
                  Mark:1101,
                  Grade: "S"
               },
               {
                  Name:"suresh kumar",
                  Mark:751,
                  Grade: "D"
               }
            ]
         });
      }
      changeName = (newName) => {
         this.setState({
            Name: newName
         });
      }
      changeNameFromInput = (event) => {
         this.setState({
            Name: event.target.value
         });
      }
   render(){
      const data = <Home />
      return(
         // <div>
         //    <h2>state page</h2>
         //    <StateDetail name="pml">
         //       i am children
         //    </StateDetail>
         //    <button onClick={this.changeMark}>click me</button>
         // </div>
         <div>
            <h1>{this.state.Name}</h1>
            <button onClick={()=>this.changeName("pml")}>Change Name use anon fun</button>
            <br/>
            <br/>
            <button onClick={this.changeName.bind(this,"laxman bind")}>Change Name use bind fun</button>
            <br/>
            <br/>
            <input type="text" onChange={this.changeNameFromInput} value={this.state.Name}/>
         </div>
      );
   }
}
export default StatePage;