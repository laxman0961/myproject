import React, { Component } from 'react';
import { observer } from "mobx-react"
import store from './TestStore';
import { Link } from 'react-router-dom'
@observer
class Header extends Component{
   render(){
      return(
         <nav>
            <h2>Header</h2>
            <ul>
               <Link to="/">
                  <li>home</li>
               </Link>
               <Link to="/about">
                  <li>about</li>
               </Link>
               <Link to="/State">
                  <li>State</li>
               </Link>
               <Link to="/mobx">
                  <li>mobx</li>
               </Link>
            </ul>
         </nav>
      );
   }
}
export default Header;