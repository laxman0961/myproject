import React, { Component } from 'react';
import { observer } from "mobx-react"
import store from './TestStore';

class Home extends Component{

   render(){
      return(
         <div>
            <h2>home page</h2>
         </div>
      );
   }
}
export default Home;