import React, { Component } from 'react';
import { observer } from "mobx-react"
import store from './TestStore';

@observer
class Test extends Component{
   handleclick = () => {
      this.props.store.count = this.props.store.count + 1
   }
   render(){
      return(
         <div>
            <h2>pml</h2>
            <h1>{this.props.store.count}</h1>
            <button onClick={this.handleclick}>clickme</button>
         </div>
      );
   }
}
export default Test;